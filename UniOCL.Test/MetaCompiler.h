// This file was created by UniOCL_MetaCompiler by Leon Gierschner!
// Do not modify this file directly!
#pragma once
#include <vector>
#include <Kernel.h>
#include <ClMemoryFlag.h>
namespace meta
{
namespace test
{
struct Add : public Kernel                                     
{                                                                 
   Add (int KERNEL_SIZE, bool ENABLED) : Kernel( BUFFERFLOW( IN, IN, IN, OUT ) )  
   {                                                              
                                                         
      Compile ("KERNEL_SIZE, ENABLED", KERNEL_SIZE, ENABLED);                              
   }                                                              
                                                                  
   ClKernel define_kernel() const override                        
   {                                                              
      return ClKernel( "Add" , KERNEL(                       
                                                                  
                                                                  
      kernel void Add(global const int* a, global const int* b, global const int* c, global int* out)      {
         if (!ENABLED) return;

         int i = get_global_id(0);
         out[i] += a[i] * KERNEL_SIZE;
         out[i] += b[i];
         out[i] += c[i];
      } 
                                                          
                                                                  
                                                                  
       ) );                                                       
   }                                                              
                                                                  
   void operator()( unsigned long long workers, std::vector<int>& a, std::vector<int>& b, std::vector<int>& c, std::vector<int>& out )          
   { __super::operator()(workers, a, b, c, out ); }                
};                                                                
}
}

