#define BOOST_TEST_MODULE UniOCLTest

#include <boost/test/unit_test.hpp>

#include "../UniOCL/UniOCL.h"
#include "../UniOCL/ClOption.h"
#include "../UniOCL/ClProgram.h"
#include "../UniOCL/ClKernel.h"
#include "../UniOCL/ClDeviceType.h"
#include "../UniOCL/ClMemoryFlag.h"
#include "../UniOCL/Kernel.h"

#include <iostream>
#include <string>
#include <typeinfo>

namespace
{
   void print(std::string mes)
   {
      BOOST_TEST_MESSAGE(mes);
      std::cout << mes << std::endl;
   }

   template<typename T>
   void print(std::vector<T> mes)
   {
      for (auto& item : mes)
         BOOST_TEST_MESSAGE(item);
   }
}

BOOST_AUTO_TEST_CASE(GetDefaultClPlatformName)
{
   print("GetDefaultClPlatformName");
   print(UniOCL::getDefaultPlatformName().toStdString());
   print("end");
}

BOOST_AUTO_TEST_CASE(GetAllClPlatformNames)
{
   print("GetAllClPlatformNames");
   for (auto& platformName : UniOCL::getAllPlatformNames())
      print(platformName.toStdString());
   print("end");
}

BOOST_AUTO_TEST_CASE(GetDefaultClDeviceName)
{
   print("GetDefaultClDeviceName");
   print(UniOCL::getDefaultDeviceName(ClDeviceType::ALL).toStdString());
   print("end");
}

BOOST_AUTO_TEST_CASE(GetAllClDeviceNames)
{
   print("GetAllClDeviceNames");
   for (auto& deviceName : UniOCL::getAllDeviceNames(ClDeviceType::ALL))
      print(deviceName.toStdString());
   print("end");
}

BOOST_AUTO_TEST_CASE(EasyCreationOfKernelProgram)
{
   // creating kernel
   ClKernel kernel("VectorAddition");
   kernel.loadKernelCodeFromFile("VectorAddition.cl");

   // creating operation options
   ClOption options(kernel);
   options.setDevice(ALL);

   // creating and compiling program
   print(kernel.getKernelCode());
   auto program = UniOCL::createProgram(options,
      { IN, IN, OUT, OUT });

   // running program with specified values
   int listSize = 100;
   int byteSize = sizeof(int) * listSize;
   std::vector<int> a = std::vector<int>(listSize);
   std::vector<int> b = std::vector<int>(listSize);
   std::vector<int> c = std::vector<int>(listSize);
   int* res = new int[listSize];
   int test = 0;

   for (int n = 0; n < listSize; n++)
   {
      a[n] = n; b[n] = n; res[n] = 2 * n;
   }

   ClInput input;
   input.add(a).add(b).add(c).add(test);;
   //auto result = UniOCL::runProgram("VectorAddition", listSize, { byteSize, byteSize, byteSize }, a, b, c);
   program(listSize, input);

   BOOST_CHECK_EQUAL(test, 1);

   // comparing results
   BOOST_CHECK_EQUAL_COLLECTIONS(c.begin(), c.end(), res, res + listSize);

   delete[] res;
}

BOOST_AUTO_TEST_CASE(HomogeneousCodeCreation)
{
   ClKernel kernel("MajorCalc");
   kernel.setKernelCode(DEFINE_KERNEL(
      kernel void MajorCalc(global const int* buffer, global int* out)
   {
      int id = get_global_id(0);
      out[id] = buffer[id];

      SYNC;
   }));

   ClOption options(kernel);
   options.setDevice(ALL);

   print(kernel.getKernelCode());
   auto program = UniOCL::createProgram(options,
      { ClMemoryFlag::DeviceRead	, ClMemoryFlag::DeviceWrite },
      { ClMemoryFlag::HostWrite	, ClMemoryFlag::HostRead });

   if (program.hasError())
      print(program.getErrorLog());
}

BOOST_AUTO_TEST_CASE(HomogeneousCodeKernelCreation)
{

   CREATE_KERNEL_CLASS(TestKernel, Test,
      kernel void Test(global const int* buf)
   {
      buf[get_global_id(0)] = 0;
      SYNC;
   };
   )

      TestKernel kernel;

   ClOption options(kernel);
   options.setDevice(ALL);

   print(kernel.getKernelCode());
   auto program = UniOCL::createProgram(options,
      { ClMemoryFlag::DeviceRead	, ClMemoryFlag::DeviceWrite },
      { ClMemoryFlag::HostWrite	, ClMemoryFlag::HostRead });

   if (program.hasError())
      print(program.getErrorLog());
}

BOOST_AUTO_TEST_CASE(VectorAdditionUsingHomogeneousKernelClass)
{
   CREATE_KERNEL_CLASS(VectorAddition, AddVectors,
      kernel void AddVectors(global const float* a, global const float* b, global float* out)
   {
      int id = get_global_id(0);
      out[id] = a[id] + b[id];
   }
   );

   ClOption options(new VectorAddition());
   options.setDevice(CPU);

   print(options.getKernelCode());
   auto program = UniOCL::createProgram(options, { IN, IN, OUT });

   if (program.hasError())
      print(program.getErrorLog());

   std::vector<float> a = { 1, 2, 3 };
   std::vector<float> b = { 4, 5, 6 };
   std::vector<float> c(3);
   std::vector<float> ref = { 5, 7, 9 };

   ClInput input;
   input.add(a).add(b).add(c);

   program(3, input);

   BOOST_CHECK_EQUAL_COLLECTIONS(ref.begin(), ref.end(), c.begin(), c.end());
}

BOOST_AUTO_TEST_CASE(KernelInheritanceWorks)
{
   struct TestBase1 : public Kernel
   {
      TestBase1() : Kernel({ IN, IN, OUT })
      {
      }

      ClKernel define_kernel() const override
      {
         return ClKernel("", "a..");
      };
   };

   struct TestBase2 : public Kernel
   {
      TestBase2() : Kernel({ IN, IN, OUT })
      {
      }

      ClKernel define_kernel() const override
      {
         return ClKernel("", "b..");
      };
   };

   struct Test : public Kernel
   {
      Test() : Kernel({ IN, IN, OUT })
      {
         includes<TestBase1, TestBase2>();
      }

      ClKernel define_kernel() const override
      {
         return ClKernel("", "c..");
      };
   };

   print(Test().getKernelCode());
}

BOOST_AUTO_TEST_CASE(KernelCallWorks)
{
   struct add : public Kernel
   {
      add() : Kernel({ IN, IN, OUT })
      {
         Compile();
      }

      ClKernel define_kernel() const override
      {
         return ClKernel("add", KERNEL(
            kernel void add(global const int* a, global const int* b, global int* c)
         {
            int id = get_global_id(0);
            c[id] = a[id] + b[id];
         }
         ));
      }
   };

   add t;
   std::vector<int> a = { 1, 2, 3 };
   std::vector<int> b = { 1, 2, 3 };
   std::vector<int> c(3);
   std::vector<int> ref = { 2, 4, 6 };
   t(3, a, b, c);
   BOOST_CHECK_EQUAL_COLLECTIONS(ref.begin(), ref.end(), c.begin(), c.end());
   print(c);
}

BOOST_AUTO_TEST_CASE(KernelCallInheritanceWorks)
{
   CL_KERNEL(add, BUFFERFLOW(IN, IN, OUT),
      kernel void add(global const int* a, global const int* b, global int* c)
   {
      int id = get_global_id(0);
      c[id] = a[id] + b[id];
   });

   CL_KERNEL_WITH_INCLUDES(addV, BUFFERFLOW(IN, IN, OUT),
      kernel void addV(global const int* a, global const int* b, global int* c)
   {
      int id = get_global_id(0);
      add(a, b, c);
   }
   , add);

   addV t;
   std::vector<int> a = { 1, 2, 3 };
   std::vector<int> b = { 1, 2, 3 };
   std::vector<int> c(3);
   std::vector<int> ref = { 2, 4, 6 };
   t(3, a, b, c);
   BOOST_CHECK_EQUAL_COLLECTIONS(ref.begin(), ref.end(), c.begin(), c.end());
   print(c);
}

#include "MetaCompiler.h"
BOOST_AUTO_TEST_CASE(MetaCompilerWorks)
{
   using namespace meta::test;
   Add add(3, true);

   std::vector<int> a = { 1, 2, 3 };
   std::vector<int> b = { 1, 2, 3 };
   std::vector<int> c = { 1, 2, 3 };
   std::vector<int> out(3);
   std::vector<int> ref = { 5, 10, 15 };

   add(3, a, b, c, out);

   BOOST_CHECK_EQUAL_COLLECTIONS(ref.begin(), ref.end(), out.begin(), out.end());
}

#include "Positioning.h"
BOOST_AUTO_TEST_CASE(RealScenarioCasePositioning)
{
   using namespace maths::positioning;
   UpdateBalls balls(1.5f);

   std::vector<float> _balls = {1, 2, 3, 4};
   std::vector<float> _velcs = {1, 2, 3, 4};
   std::vector<float> ref = {2.5f, 5, 7.5f, 10};
   balls(2, _balls, _velcs, _balls);

   BOOST_CHECK_EQUAL_COLLECTIONS(ref.begin(), ref.end(), _balls.begin(), _balls.end());
}

BOOST_AUTO_TEST_CASE(MajorCalcCompilesCorrectly)
{
   ClKernel kernel("MajorCalc");
   kernel.loadKernelCodeFromFile("MajorCalc.cl");

   ClOption options(kernel);
   options.setDevice(ALL);

   print(kernel.getKernelCode());
   UniOCL::createProgram(options,
      { ClMemoryFlag::DeviceRead	, ClMemoryFlag::DeviceWrite },
      { ClMemoryFlag::HostWrite	, ClMemoryFlag::HostRead });
}

BOOST_AUTO_TEST_CASE(MajorCalcIsFast)
{
   ClKernel kernel("MajorCalc");
   kernel.loadKernelCodeFromFile("MajorCalc.cl");

   ClOption options(kernel);
   options.setDevice(ALL);

   print(kernel.getKernelCode());
   auto program = UniOCL::createProgram(options,
      { ClMemoryFlag::DeviceRead	, ClMemoryFlag::DeviceWrite },
      { ClMemoryFlag::HostWrite	, ClMemoryFlag::HostRead });

   int listSize = 10000;
   int byteSize = sizeof(float) * listSize;
   float* in = new float[listSize];
   float* out = new float[listSize];

   for (int n = -listSize / 2; n < listSize / 2; n++)
      in[n + listSize / 2] = n * .01f;

   //UniOCL::runProgram("MajorCalc", listSize, { byteSize, byteSize }, in, out);
   ClInput input;
   input.add(in, sizeof(float) * listSize).add(out, sizeof(float) * listSize);
   program(listSize, input);

   delete[] in;
   delete[] out;
}