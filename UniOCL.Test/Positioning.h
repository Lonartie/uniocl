// This file was created by UniOCL_MetaCompiler by Leon Gierschner!
// Do not modify this file directly!
#pragma once
#include <vector>
#include <Kernel.h>
#include <ClMemoryFlag.h>
namespace maths
{
   namespace positioning
   {
      struct UpdateBalls : public Kernel
      {
         UpdateBalls(float DELTA_TIME) : Kernel(BUFFERFLOW(IN, IN, OUT))
         {

            Compile("DELTA_TIME", DELTA_TIME);
         }

         ClKernel define_kernel() const override
         {
            return ClKernel("UpdateBalls", KERNEL(


               kernel void UpdateBalls(global const float* positions, global const float* velocities, global float* updated_positions)            {
               int i = get_global_id(0);
               float x = positions[2 * i + 0];
               float y = positions[2 * i + 1];
               float vx = velocities[2 * i + 0];
               float vy = velocities[2 * i + 1];
               updated_positions[2 * i + 0] = x + vx * DELTA_TIME;
               updated_positions[2 * i + 1] = y + vy * DELTA_TIME;
            }



            ));
         }

         void operator()(unsigned long long workers, std::vector<float>& positions, std::vector<float>& velocities, std::vector<float>& updated_positions)
         {
            __super::operator()(workers, positions, velocities, updated_positions);
         }
      };
   }
}

