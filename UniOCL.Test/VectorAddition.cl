kernel void VectorAddition(global const int* a, global const int* b, global int* c, global int* test) 
{
	if (get_global_id(0) == 0)
		*test = 1;
	c[get_global_id(0)] = a[get_global_id(0)] + b[get_global_id(0)]; 
}