
namespace meta
{ 
   namespace test
   {
      [requires<int>(KERNEL_SIZE)]
      [requires<bool>(ENABLED)]      
      kernel void Add(global const int* a, global const int* b, global const int* c, global int* out)
      {
         if (!ENABLED) return;

         int i = get_global_id(0);
         out[i] += a[i] * KERNEL_SIZE;
         out[i] += b[i];
         out[i] += c[i];
      } 
   }
}