
namespace maths
{
   namespace positioning
   {
      [requires<float>(DELTA_TIME)]
      kernel void UpdateBalls(global const float* positions, global const float* velocities, global float* updated_positions)
      {
         int i = get_global_id(0);
         float x = positions[2 * i + 0];
         float y = positions[2 * i + 1];
         float vx = velocities[2 * i + 0];
         float vy = velocities[2 * i + 1];
         updated_positions[2 * i + 0] = x + vx * DELTA_TIME;
         updated_positions[2 * i + 1] = y + vy * DELTA_TIME;
      }
   }
}