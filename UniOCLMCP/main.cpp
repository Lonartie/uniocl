#include <iostream>

#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QStringList>

#include "../UniOCL/UniOCL.h"


#define _String(x) #x
#define STRING(x) _String(x)
#define print(x) std::cout << x << std::endl
#define wait() std::cin.get()

bool compilant = true;
QString build_logs = "";
QString ParseInput(const QString& content);
int main(int argc, char *argv[])
{
   if (argc < 2) { print("no file specified!"); return 2; }
   QString filepath(argv[1]);

   QFileInfo fileinfo(filepath);
   QFile file(fileinfo.absoluteFilePath());
   file.open(QIODevice::ReadOnly);
   QString content(file.readAll());
   file.close();

   QFile output(fileinfo.dir().path() + "/" + fileinfo.completeBaseName() + ".h");
   output.open(QIODevice::WriteOnly);

   QString out = ParseInput(content);
   output.write(out.toUtf8(), out.size());
   output.flush();
   output.close();

   if (!compilant)
   {
      print(("warning: build error in kernel\n" + build_logs).toStdString());      
   }
}

extern QString file_template;
QString create_class(QStringList nmspaces, QStringList includes, QString fname, QStringList args, QString kernel, QStringList macros);
QString build(QString kernel_code, QStringList args, QString fname);
QString ParseInput(const QString& content)
{
   QString result = "";
   QStringList nmspaces = {};
   QStringList includes = {};
   QString fname = "";
   QStringList args = {};
   QString kernel_code = "";
   QStringList macros;

   bool begin_kernel = false;
   int kernel_catch = 0;

   bool begin_namespace = false;
   int namespace_catch = 0;

   for (auto& line : content.split("\n"))
   {
      bool begin_k = false;
      bool begin_n = false;

      if (line.contains("requires") && line.contains("[") && line.contains("<"))
      {
         macros.push_back(line);
      }

      if (line.contains("#include"))
      {
         result = "#include \"" + line.split("\"")[1].split(";").first().trimmed() + "\"\n" + result;
         includes.append(line.split("\"")[1].split(";").last().trimmed());
      }

      if (line.contains("namespace"))
      {
         nmspaces.append(line.trimmed().split(" ").last());
         begin_n = true;
      }

      if (begin_namespace)
      {
         namespace_catch += line.count("{");
         namespace_catch -= line.count("}");
      }

      if (begin_namespace && namespace_catch == 0)
      {
         namespace_catch = 0;
         begin_namespace = false;
         nmspaces.removeLast();
      }

      if (line.toLower().contains("kernel void"))
      {
         begin_k = true;
         kernel_code += line;
         fname = line.split("void").last().split("(").first().trimmed();
         args = line.split("(").last().split(")").first().remove("global", Qt::CaseInsensitive).trimmed().split(",");
      }

      if (begin_kernel)
      {
         kernel_catch += line.count("{");
         kernel_catch -= line.count("}");
         kernel_code += line + "\n";
      }

      if (begin_kernel && kernel_catch == 0)
      {
         result += create_class(nmspaces, includes, fname, args, kernel_code, macros) + "\n";
         macros.clear();
         build_logs += build(kernel_code, args, fname);
         begin_kernel = false;
         kernel_catch = 0;
         fname = "";
         kernel_code = "";
         args = QStringList();
      }

      if (begin_k)
         begin_kernel = true;
      if (begin_n)
         begin_namespace = true;
   }

   return file_template + result;
}

extern QString class_template;
QString create_class(QStringList nmspaces, QStringList includes, QString fname, QStringList args, QString kernel, QStringList macros)
{
   QString namespace_prefix = "";
   QString namespace_suffix = "";
   QString class_str = class_template;
   QString arg_list = "";
   QString arg_names = "";
   QString flow = "";
   QString name_str = "\"" + fname + "\"";
   QString incs = (includes.size() != 0 ? "include<" : "");
   QString macronames = "";
   QString macroargs = "";

   for (auto& macro : macros)
   {
      auto current_name = (macro.split("(").last().split(")").first());
      macronames += current_name + QString(macro == macros.last() ? "" : ", ");
      macroargs += (macro.split("<").last().split(">").first()) + " " + current_name + QString(macro == macros.last() ? "" : ", ");
   }

   if (!macros.empty())
      macronames = "\"" + macronames + "\", " + macronames;

   for (auto& nmspace : nmspaces)
   {
      namespace_prefix += "namespace " + nmspace + "\n{\n";
      namespace_suffix += "}\n";
   }

   for (auto& arg : args)
   {
      bool in = QString(arg).contains("const");
      QString arg_name = QString(arg).trimmed().split(" ").last();
      QString arg_tp = QString(arg).remove("global").remove("const").remove("*");
      auto tps = arg_tp.split(" ");
      arg_tp = "";

      for (auto& tp : tps)
         if (tp != tps.last())
            arg_tp += tp + " ";

      arg_list += "std::vector<" + arg_tp.trimmed() + ">& " + arg_name + (arg != args.last() ? ", " : "");
      flow += QString((in ? "IN" : "OUT")) + QString((arg != args.last() ? ", " : ""));
      arg_names += arg_name + (arg != args.last() ? ", " : "");
   }

   for (auto& inc : includes)
   {
      incs += inc + (inc != includes.last() ? ", " : ">();");
   }

   class_str.replace("$NAME$", fname);
   class_str.replace("$NAME_STR$", name_str);
   class_str.replace("$ARGS$", arg_list);
   class_str.replace("$INCLUDE$", incs);
   class_str.replace("$ARG_NAMES$", arg_names);
   class_str.replace("$KERNEL$", kernel);
   class_str.replace("$BUFF_FLOW$", flow);
   class_str.replace("$COMPILE_ARGS$", macroargs);
   class_str.replace("$COMPILE_ARG_NAMES$", macronames);

   return QString(namespace_prefix + class_str + namespace_suffix);
}

QString build(QString kernel_code, QStringList args, QString fname)
{
   std::vector<int> buffer;

   for (auto& arg : args)
      if (arg.contains("const"))
         buffer.push_back(IN);
      else
         buffer.push_back(OUT);

   ClKernel kernel(fname.toStdString(), kernel_code.toStdString());
   ClOption options(kernel);
   options.setDevice(ALL);
   auto program = UniOCL::createProgram(options, buffer);
   if (program.hasError())
   {
      compilant = false;
      return QString::fromStdString(program.getErrorLog()) + "\n";
   }
   return "";
}

QString class_template =
QString("struct $NAME$ : public Kernel                                     \n") +
QString("{                                                                 \n") +
QString("   $NAME$ ($COMPILE_ARGS$) : Kernel( BUFFERFLOW( $BUFF_FLOW$ ) )  \n") +
QString("   {                                                              \n") +
QString("      $INCLUDE$                                                   \n") +
QString("      Compile ($COMPILE_ARG_NAMES$);                              \n") +
QString("   }                                                              \n") +
QString("                                                                  \n") +
QString("   ClKernel define_kernel() const override                        \n") +
QString("   {                                                              \n") +
QString("      return ClKernel( $NAME_STR$ , KERNEL(                       \n") +
QString("                                                                  \n") +
QString("                                                                  \n") +
QString("$KERNEL$                                                          \n") +
QString("                                                                  \n") +
QString("                                                                  \n") +
QString("       ) );                                                       \n") +
QString("   }                                                              \n") +
QString("                                                                  \n") +
QString("   void operator()( unsigned long long workers, $ARGS$ )          \n") +
QString("   { __super::operator()(workers, $ARG_NAMES$ ); }                \n") +
QString("};                                                                \n");





QString file_template =
QString("// This file was created by UniOCL_MetaCompiler by Leon Gierschner!\n") +
QString("// Do not modify this file directly!\n") +
QString("#pragma once\n") +
QString("#include <vector>\n") +
QString("#include <Kernel.h>\n") +
QString("#include <ClMemoryFlag.h>\n");
