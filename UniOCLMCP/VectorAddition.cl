kernel void VectorAddition(global const float* a, global const float* b, global float* out)
{
   int i = get_global_id(0);
   out[i] = a[i] + b[i];
}