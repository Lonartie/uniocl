#include "UniOCL.h"

std::map<std::string, void*> UniOCL::m_allPrograms;

QString UniOCL::getDefaultPlatformName()
{
	return getAllPlatformNames()[0];
}

std::vector<QString> UniOCL::getAllPlatformNames()
{
	std::vector<QString> platformNames;
	for (auto& platform : getAllPlatforms())
		platformNames.push_back(QString(platform.getInfo<CL_PLATFORM_NAME>().c_str()));
	return platformNames;
}

QString UniOCL::getDefaultDeviceName(ClDeviceType deviceType)
{
	return getAllDeviceNames(deviceType)[0];
}

std::vector<QString> UniOCL::getAllDeviceNames(ClDeviceType deviceType)
{
	std::vector<QString> deviceNames;
	for (auto& device : getAllDevices(deviceType))
		deviceNames.push_back(QString(device.getInfo<CL_DEVICE_NAME>().c_str()));
	return deviceNames;
}

ClProgram UniOCL::createProgram(ClOption programOptions,
	std::vector<int> deviceFlags,
	std::vector<int> mainFlags)
{
	if (mainFlags.size() == 0)
		for (auto& flag : deviceFlags)
			mainFlags.push_back(flag == int(IN) ? ClMemoryFlag::HostWrite : flag == int(OUT) ? ClMemoryFlag::HostRead : ClMemoryFlag::HostNoAccess);

   const char* options = programOptions.getKernelOptions().c_str();
   bool opt_enabled = programOptions.getKernelOptions().size() > 0;
	cl::Context* context = new cl::Context({ programOptions.getDevice() });
	cl::Program::Sources* sources = new cl::Program::Sources();
	auto kernelCode = programOptions.getKernelCode();
	sources->push_back({ kernelCode.c_str(), kernelCode.size() });

	cl::Program* program = new cl::Program(*context, *sources);
   if (opt_enabled)
   {
      if (program->build({ programOptions.getDevice() }, options) != CL_SUCCESS)
      {
         return ClProgram(program->getBuildInfo<CL_PROGRAM_BUILD_LOG>(programOptions.getDevice()));
      }
   }
   else
   {
      if (program->build({ programOptions.getDevice() }) != CL_SUCCESS)
      {
         return ClProgram(program->getBuildInfo<CL_PROGRAM_BUILD_LOG>(programOptions.getDevice()));
      }
   }

	ClProgram* clprogram = new ClProgram(programOptions.getKernelName(), context, programOptions.getDevice(), program, deviceFlags, mainFlags);

	m_allPrograms.emplace(programOptions.getKernelName(), clprogram);

	return *clprogram;
}

cl::Platform UniOCL::getDefaultPlatform()
{
	return getAllPlatforms()[0];
}

std::vector<cl::Platform> UniOCL::getAllPlatforms()
{
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	return platforms;
}

cl::Device UniOCL::getDefaultDevice(ClDeviceType deviceType)
{
	return getAllDevices(deviceType)[0];
}

std::vector<cl::Device> UniOCL::getAllDevices(ClDeviceType deviceType)
{
	std::vector<cl::Device> devices;
	for (auto& platform : getAllPlatforms())
	{
		std::vector<cl::Device> devicesInPlatform;
		platform.getDevices(getType(deviceType), &devicesInPlatform);
		for (auto& device : devicesInPlatform)
			devices.push_back(device);
	}
	return devices;
}

//std::vector<double> UniOCL::executeKernelCode(std::vector<cl::Device> devices, std::vector<std::string> kernelCodes)
//{
//	cl::Context context({ devices[0] });
//	cl::Program::Sources sources;
//	for (auto& code : kernelCodes)
//		sources.push_back({ code.c_str(), code.size() });
//
//	cl::Program program(context, sources);
//	if (program.build({ devices[0] }) != CL_SUCCESS)
//		throw std::exception (("Error building: " + program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]) + "\n").c_str());
//	
//	cl::Buffer buffer_A(context, CL_MEM_READ_WRITE, sizeof(double) * 10);
//	cl::Buffer buffer_B(context, CL_MEM_READ_WRITE, sizeof(double) * 10);
//	cl::Buffer buffer_C(context, CL_MEM_READ_WRITE, sizeof(double) * 10);
//
//	double A[] = { 0, 1, 2, 3, 4.23412343, 5, 6, 7, 8, 9 };
//	double B[] = { 0.123, 1, 2, 0.002, 1.1239084, 2, 12.23133, 1, 2, 0.12318974 };
//
//	cl::CommandQueue queue(context, devices[0]);
//
//	queue.enqueueWriteBuffer(buffer_A, CL_TRUE, 0, sizeof(double) * 10, A);
//	queue.enqueueWriteBuffer(buffer_B, CL_TRUE, 0, sizeof(double) * 10, B);
//
//	cl::Kernel kernel_add = cl::Kernel(program, "simple_add");
//	kernel_add.setArg(0, buffer_A);
//	kernel_add.setArg(1, buffer_B);
//	kernel_add.setArg(2, buffer_C);
//	queue.enqueueNDRangeKernel(kernel_add, cl::NullRange, cl::NDRange(10), cl::NullRange);
//	queue.finish(); 
//	
//	double C[10];
//	queue.enqueueReadBuffer(buffer_C, CL_TRUE, 0, sizeof(double) * 10, C);
//
//	return std::vector<double> (&C[0], &C[10]);
//}

cl_device_type UniOCL::getType(ClDeviceType deviceType)
{
	switch (deviceType)
	{
	case ALL:
		return CL_DEVICE_TYPE_ALL;
		break;
	case CPU:
		return CL_DEVICE_TYPE_CPU;
		break;
	case GPU:
		return CL_DEVICE_TYPE_GPU;
		break;
	case DEFAULT:
		return CL_DEVICE_TYPE_DEFAULT;
		break;
	case CUSTOM:
		return CL_DEVICE_TYPE_CUSTOM;
		break;
	case ACCELERATOR:
		return CL_DEVICE_TYPE_ACCELERATOR;
		break;
	}
	return CL_DEVICE_TYPE_ALL;
}
