#pragma once
#include "uniocl_global.h"

#define IN int(ClMemoryFlag::DeviceRead)
#define OUT int(ClMemoryFlag::DeviceWrite)

enum UNIOCL_EXPORT ClMemoryFlag
{
	DeviceReadWrite = (1 << 0),
	DeviceRead = (1 << 2),
	DeviceWrite = (1 << 1),
	HostRead = (1 << 8),
	HostWrite = (1 << 7),
	HostNoAccess = (1 << 9)
};