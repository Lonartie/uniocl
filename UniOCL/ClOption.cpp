#include "ClOption.h"
#include "UniOCL.h"

ClOption::ClOption(ClKernel* kernel)
   : m_kernel(kernel)
{
}

ClOption::ClOption(ClKernel& kernel)
   : m_kernel(std::make_shared<ClKernel>(kernel))
{
}

cl::Device ClOption::getDevice()
{
	return m_selectedDevice;
}

std::string ClOption::getKernelCode()
{
	return m_kernel->getKernelCode();
}

std::string ClOption::getKernelName()
{
	return m_kernel->getKernelName();
}

void ClOption::setKernelOptions(const std::string& options)
{
   this->options = options;
}

const std::string& ClOption::getKernelOptions() const
{
   return options;
}

void ClOption::setDevice(ClDeviceType deviceType)
{
	m_selectedDevice = UniOCL::getDefaultDevice(deviceType);
}
