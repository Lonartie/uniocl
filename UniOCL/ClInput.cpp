#include "ClInput.h"

ClInput & ClInput::add(void* data, unsigned long long size)
{
	this->data.push_back(data);
	this->size.push_back(size);
	return *this;
}

std::vector<void*>& ClInput::getData()
{
	return data;
}

std::vector<unsigned long long>& ClInput::getSize()
{
	return size;
}
