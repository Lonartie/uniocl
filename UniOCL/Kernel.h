#pragma once

#include "uniocl_global.h"
#include "UniOCL.h"
#include "ClKernel.h"
#include "ClDeviceType.h"
#include "ClMemoryFlag.h"
#include <utility>
#include <vector>
#include <memory>
#include <QtCore/QStringList>

class UNIOCL_EXPORT Kernel
{
public:

   Kernel(std::vector<int> buffer_flow);

   template<typename ...Args>
   void Compile(std::string names, Args...args);

   void Compile();

   template<typename ...Args>
   void operator()(unsigned long long workers, Args&... args);

   std::string getKernelCode() const;

   virtual ClKernel define_kernel() const = 0;

protected:

   template<typename ...Classes>
   void includes();

private:

   template<typename T>
   static T create_instance();

   template<int N = 0, typename First, typename ...Rest, typename = typename std::enable_if<N >= 2>::type>
   static ClInput collect_input(First& first, Rest&... rest);
   
   template<int N = 1, typename First>
   static ClInput collect_input(First& first);

   template<int N = 0, typename First, typename ...Rest, typename = typename std::enable_if<N >= 2>::type>
   static std::vector<std::string> collect();

   template<int N = 1, typename First>
   static std::vector<std::string> collect();

   template<int N = 0, typename First, typename ...Rest, typename = typename std::enable_if<N >= 2>::type>
   std::vector<std::string> get_args(First first, Rest...rest);

   template<int N = 1, typename First>
   std::vector<std::string> get_args(First first);

   std::string base_code;

   ClDeviceType device = ALL;

   std::vector<int> buffer_flow;

   std::shared_ptr<ClProgram> program;

};

template<typename ...Args>
void Kernel::Compile(std::string names, Args...args)
{
   std::vector<std::string> _args = get_args<sizeof...(Args), Args...>(args...);

   std::string all_args = "";
   QString _names = QString::fromStdString(names);
   int index = 0;
   for (auto& arg : _args)
      all_args += "-D " + (_names.split(",")[index++]).toStdString() + "=" + arg + " ";

   ClKernel kernel(this->define_kernel().getKernelName(), getKernelCode());
   ClOption options(kernel);
   options.setKernelOptions(all_args);
   
   options.setDevice(device);

   program = std::make_shared<ClProgram>(UniOCL::createProgram(options, buffer_flow));
}

template <typename ... Args>
void Kernel::operator()(unsigned long long workers, Args&... args)
{
   ClInput input(collect_input<sizeof...(Args), Args...>(args...));
   (*program)(workers, input);
}

template <typename ... Classes>
void Kernel::includes()
{
   std::vector<std::string> kernel_codes = collect<sizeof...(Classes), Classes...>();
   for (auto& kernel_code : kernel_codes)
      base_code += kernel_code;
}

template <typename T>
T Kernel::create_instance()
{
   return T{};
}

template <int N, typename First, typename ... Rest, typename>
ClInput Kernel::collect_input(First& first, Rest&... rest)
{
   ClInput all = collect_input<1, First>(first);
   ClInput _rest = collect_input<sizeof...(Rest), Rest...>(rest...);
   for (int n = 0; n < _rest.getSize().size(); n++)
      all.add(_rest.getData()[n], _rest.getSize()[n]);
   return all;
}

template <int N, typename First>
ClInput Kernel::collect_input(First& first)
{
   ClInput input;
   input.add(first);
   return input;
}

template <int N, typename First, typename ... Rest, typename>
std::vector<std::string> Kernel::collect()
{
   std::vector<std::string> kernel_codes;
   First first(create_instance<First>());
   kernel_codes.push_back(first.getKernelCode());
   for (auto& kernel_code : collect<sizeof...(Rest), Rest...>())
      kernel_codes.push_back(kernel_code);
   return kernel_codes;
}

template <int N, typename First>
std::vector<std::string> Kernel::collect()
{
   return { create_instance<First>().getKernelCode() };
}

template <int N, typename First, typename ... Rest, typename>
std::vector<std::string> Kernel::get_args(First first, Rest... rest)
{
   std::vector<std::string> all = get_args<1, First>(first);
   for (auto& item : get_args<sizeof...(Rest), Rest...>(rest...))
      all.push_back(item);
   return all;
}

template <int N, typename First>
std::vector<std::string> Kernel::get_args(First first)
{
   return { std::to_string(first) };
}

#define CL_KERNEL_WITH_INCLUDES(Name, BufferFlow, TKernel, ...)       \
struct Name : public Kernel                                           \
{                                                                     \
   Name () : Kernel( BufferFlow )                                     \
   {                                                                  \
      includes< __VA_ARGS__ >();                                      \
      Compile();                                                      \
   }                                                                  \
                                                                      \
   ClKernel define_kernel() const override                            \
   {                                                                  \
      return ClKernel( #Name , KERNEL( TKernel ) );                   \
   }                                                                  \
};

#define CL_KERNEL(Name, BufferFlow, TKernel)                          \
struct Name : public Kernel                                           \
{                                                                     \
   Name () : Kernel( BufferFlow )                                     \
   {                                                                  \
      Compile();                                                      \
   }                                                                  \
                                                                      \
   ClKernel define_kernel() const override                            \
   {                                                                  \
      return ClKernel( #Name , KERNEL( TKernel ) );                   \
   }                                                                  \
};

#define BUFFERFLOW(...) std::vector<int> { __VA_ARGS__ }