#pragma once

#include "uniocl_global.h"

#include "ClProgram.h"
#include "ClMemoryFlag.h"
#include "ClInput.h"

#include <CL/cl.hpp>
#include <vector>
#include <map>
#include <iostream>

class ClProgram
{
public:

   ClProgram(std::string error)
   {
      
   }

	ClProgram(std::string kernelName, cl::Context* context, cl::Device device, cl::Program* program, std::vector<int> deviceFlags, std::vector<int> mainFlags)
		: m_program(program),
		m_context(context),
		m_device(device),
		m_deviceFlags(deviceFlags),
		m_mainFlags(mainFlags),
		m_kernelName(kernelName)
	{
		m_commandQueue = new cl::CommandQueue(*m_context, m_device, 0Ui64, &error);
		m_kernel = new cl::Kernel(*m_program, m_kernelName.c_str(), &error);
	};

	void operator() (int iterations, ClInput& inputBuffers)
	{
      for (auto& buf : m_buffers)
         delete buf.first;
      m_buffers.clear();

		//createBuffers(m_deviceFlags, argSizes, args...);
		for (int n = 0; n < inputBuffers.getData().size(); n++)
		{
			auto buffer = new cl::Buffer(
				(cl::Context) *m_context,
				(cl_mem_flags)(m_deviceFlags[n] | m_mainFlags[n]),
				(size_t) inputBuffers.getSize()[n]);

			if (m_deviceFlags[n] == ClMemoryFlag::DeviceRead)
				error = m_commandQueue->enqueueWriteBuffer(*buffer, CL_TRUE, 0, inputBuffers.getSize()[n], inputBuffers.getData()[n]);

			m_buffers.emplace_back(std::pair<cl::Buffer*, void*>(buffer, inputBuffers.getData()[n]));
		}

      int _index = 0;
		for (auto& buf : m_buffers)
			error = m_kernel->setArg(_index++, *buf.first);
		
		error = m_commandQueue->enqueueNDRangeKernel(*m_kernel, cl::NullRange, cl::NDRange(iterations), cl::NullRange);
		error = m_commandQueue->finish();

		//readBuffers(m_mainFlags, argSizes, args...);

		for (int n = 0; n < inputBuffers.getData().size(); n++)
		{
			if (m_mainFlags[n] == ClMemoryFlag::HostRead)
			{
				error = m_commandQueue->enqueueReadBuffer(*m_buffers[n].first, CL_TRUE, 0, inputBuffers.getSize()[n], inputBuffers.getData()[n]);
				//m_readBuffers.emplace(m_bufferIndex, value);
			}
		}
	};

   bool hasError() const
   {
      return !log.empty();
   }

   const std::string& getErrorLog() const
   {
      return log;
   }

private:

	template<typename First, typename...Rest> 
	void createBuffers(std::vector<int> deviceFlags, std::vector<int> argSizes, First first, Rest...rest)
	{
		createBuffers<First>({ deviceFlags[0] }, { argSizes[0] }, first);
		argSizes.erase(argSizes.begin(), argSizes.begin() + 1);
		deviceFlags.erase(deviceFlags.begin(), deviceFlags.begin() + 1);
		createBuffers<Rest...>(deviceFlags, argSizes, rest...);
	}
	
	template<typename T>
	void createBuffers(std::vector<int> deviceFlags, std::vector<int> argSizes, T value)
	{
		auto buffer = new cl::Buffer(
			(cl::Context) *m_context,
			(cl_mem_flags)(m_deviceFlags[m_bufferIndex] | m_mainFlags[m_bufferIndex]),
			(size_t)argSizes[0]);

		if (deviceFlags[0] == ClMemoryFlag::DeviceRead)
			error = m_commandQueue->enqueueWriteBuffer(*buffer, CL_TRUE, 0, argSizes[0], value);

		m_buffers.emplace(m_bufferIndex, std::pair<cl::Buffer*, void*>(buffer, value));
		m_bufferIndex++;
	}

	template<typename First, typename...Rest>
	void readBuffers(std::vector<int> mainFlags, std::vector<int> argSizes, First first, Rest...rest)
	{
		readBuffers<First>({ mainFlags[0] }, { argSizes[0] }, first);
		mainFlags.erase(mainFlags.begin(), mainFlags.begin() + 1);
		argSizes.erase(argSizes.begin(), argSizes.begin() + 1);
		readBuffers<Rest...>(mainFlags, argSizes, rest...);
	}

	template<typename T>
	void readBuffers(std::vector<int> mainFlags, std::vector<int> argSizes, T value)
	{
		if (mainFlags[0] == ClMemoryFlag::HostRead)
		{
			error = m_commandQueue->enqueueReadBuffer(*m_buffers[m_bufferIndex].first, CL_TRUE, 0, argSizes[0], value);
			m_readBuffers.emplace(m_bufferIndex, value);
		}
		m_bufferIndex++;
	}


	cl_int error = 0;

	int m_bufferIndex = 0;

	cl::CommandQueue* m_commandQueue;

	std::map<int, void*> m_readBuffers;

	std::vector<std::pair<cl::Buffer*, void*>> m_buffers;

	std::vector<int> m_deviceFlags, m_mainFlags;

	std::string m_kernelName;

   std::string log = "";

	cl::Context* m_context;

	cl::Device m_device;

	cl::Program* m_program;

	cl::Kernel* m_kernel;
};