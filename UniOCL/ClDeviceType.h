#pragma once

#include "uniocl_global.h"

enum UNIOCL_EXPORT ClDeviceType
{
	ALL = 0,
	CPU = 1,
	GPU = 2,
	CUSTOM = 3,
	ACCELERATOR = 4,
	DEFAULT = 5
};