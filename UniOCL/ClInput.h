#pragma once

#include "uniocl_global.h"

#include <vector>

class UNIOCL_EXPORT ClInput
{
public:

	ClInput() {}

	template<typename T>
	ClInput& add(T& data);

	template<typename T>
	ClInput& add(std::vector<T>& data);

	ClInput& add(void* data, unsigned long long size);

	std::vector<void*>& getData();

	std::vector<unsigned long long>& getSize();

private:

	std::vector<void*> data;

	std::vector<unsigned long long> size;
};

template<typename T>
inline ClInput & ClInput::add(T& data)
{
	this->data.push_back(&data);
	size.push_back(sizeof(T));
	return *this;
}

template<typename T>
inline ClInput & ClInput::add(std::vector<T>& data)
{
	this->data.push_back(data.data());
	size.push_back(sizeof(T) * data.size());
	return *this;
}
