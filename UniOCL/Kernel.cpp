#include "Kernel.h"

Kernel::Kernel(std::vector<int> buffer_flow)
   : buffer_flow(buffer_flow)
{}

void Kernel::Compile()
{
   ClKernel kernel(this->define_kernel().getKernelName(), getKernelCode());
   ClOption options(kernel);

   options.setDevice(device);

   program = std::make_shared<ClProgram>(UniOCL::createProgram(options, buffer_flow));
}


std::string Kernel::getKernelCode() const
{
   return base_code + "\n" + define_kernel().getKernelCode();
}

