#include "ClKernel.h"

#include "ClOption.h"

#include <fstream>
#include <streambuf>

std::map<std::string, ClKernel> ClKernel::allKernels;

ClKernel ClKernel::getKernelWithName(std::string kernelName)
{
	for (auto& kernelPair : allKernels)
		if (kernelPair.first == kernelName)
			return kernelPair.second;

	return ClKernel();
}

ClKernel::ClKernel(std::string name)
{
	ClKernel::allKernels.emplace(name, *this);
	m_kernelName = name;
}

void ClKernel::setKernelCode(std::string kernelCode)
{
	m_kernelCode = kernelCode;
}

void ClKernel::loadKernelCodeFromFile(std::string filePath)
{
	std::ifstream file(filePath);
	m_kernelCode = std::string(std::istreambuf_iterator<char>(file),
						std::istreambuf_iterator<char>());
}

std::string ClKernel::getKernelCode()
{
	return m_kernelCode;
}

std::string ClKernel::getKernelName()
{
	return m_kernelName;
}
