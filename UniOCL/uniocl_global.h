#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(UNIOCL_LIB)
#  define UNIOCL_EXPORT __declspec(dllexport)
# else
#  define UNIOCL_EXPORT __declspec(dllimport)
# endif
#else
# define UNIOCL_EXPORT
#endif
