#pragma once

#include "uniocl_global.h"

#include "ClDeviceType.h"
#include "ClKernel.h"
#include "ClOption.h"
#include "ClProgram.h"
#include "ClMemoryFlag.h"

#include <vector>
#include <QtCore/QString>

#include <CL/cl.hpp>

class UNIOCL_EXPORT UniOCL
{

public /*static*/:

	static QString getDefaultPlatformName();

	static std::vector<QString> getAllPlatformNames();

	static QString getDefaultDeviceName(ClDeviceType deviceType);

	static std::vector<QString> getAllDeviceNames(ClDeviceType deviceType);

	static cl::Platform getDefaultPlatform();

	static std::vector<cl::Platform> getAllPlatforms();

	static cl::Device getDefaultDevice(ClDeviceType deviceType);

	static std::vector<cl::Device> getAllDevices(ClDeviceType deviceType);

	//static ClProgram getProgram(std::string kernelName);

	//static void runProgram(std::string kernelName, int iterations, std::vector<int> argSizes, arg...args);

	static ClProgram createProgram(ClOption programOptions,
		std::vector<int> deviceFlags,
		std::vector<int> mainFlags = std::vector<int>(0));

private /*static*/:

	static cl_device_type getType(ClDeviceType deviceType);

	static std::map<std::string, void*> m_allPrograms;

};

//template<typename...arg>
//ClProgram<arg...> UniOCL::getProgram(std::string kernelName)
//{
//	return *static_cast<ClProgram<arg...>*>(m_allPrograms[kernelName]);
//}
//
//template<typename...arg>
//ClResult UniOCL::runProgram(std::string kernelName, int iterations, std::vector<int> argSizes, arg...args)
//{
//	ClProgram<arg...> program = getProgram<arg...>(kernelName);
//	return program(iterations, argSizes, args...);
//}