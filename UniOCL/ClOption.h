#pragma once
#include "uniocl_global.h"

#include "ClDeviceType.h"
#include "ClKernel.h"

#include <vector>
#include <map>
#include <memory>
#include <CL/cl.hpp>

class UNIOCL_EXPORT ClOption
{

public:

   ClOption(ClKernel* kernel);

	ClOption(ClKernel& kernel);

	cl::Device getDevice();

	std::string getKernelCode();

	std::string getKernelName();

   void setKernelOptions(const std::string& options);

   const std::string& getKernelOptions() const;

	void setDevice(ClDeviceType deviceType);

private:

	cl::Device m_selectedDevice;

	std::shared_ptr<ClKernel> m_kernel;

   std::string options;
};
