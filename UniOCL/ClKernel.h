#pragma once

#include "uniocl_global.h"

#include <string>
#include <typeinfo>
#include <vector>
#include <map>

class UNIOCL_EXPORT ClKernel
{

public /*static*/:

	static ClKernel getKernelWithName(std::string kernelName);

private /*static*/:

	static std::map<std::string, ClKernel> allKernels;

public:

	ClKernel() {};

	ClKernel(std::string name);

   ClKernel(const std::string& name, const std::string& kernelCode) : m_kernelName(name), m_kernelCode(kernelCode) {};

	void setKernelCode(std::string kernelCode);

	void loadKernelCodeFromFile(std::string filePath);
	
	virtual std::string getKernelCode();

	virtual std::string getKernelName();

private:
   
   std::string m_kernelName;

	std::string m_kernelCode;
	
	std::vector<const std::type_info*> m_argTypes;
};



/* KERNEL DEFINITION */

#define _DEFINE_KERNEL(kernel) #kernel
#define DEFINE_KERNEL(kernel) _DEFINE_KERNEL(kernel)
#define KERNEL(kernel) DEFINE_KERNEL(kernel) 

/* KERNEL KEYWORDS */

/// @brief [OpenCL] exposed kernel function that is callable from CPU
#define kernel kernel

/// @brief [OpenCL] exposed kernel function that is callable from CPU
#define __kernel __kernel

/// @brief [OpenCL] this parameter or variable is a global buffer
#define global global

/// @brief [OpenCL] this parameter or variable is a local buffer
#define local local

/// @brief [OpenCL] int: dimension
/// @brief [OpenCL] returns the current worker id
#define get_global_id(x) get_global_id(x)

/// @brief [OpenCL] fence_type: memory_fence
/// @brief [OpenCL] syncs all workers by only allowing to pass this function when all workers reached this point
#define barrier(x) barrier(x)
#define CLK_LOCAL_MEM_FENCE CLK_LOCAL_MEM_FENCE
#define CLK_GLOBAL_MEM_FENCE CLK_GLOBAL_MEM_FENCE
#define SYNC barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE)

#define CLKERNEL(name, kernel)                                                \
public:                                                                       \
virtual std::string getKernelCode()                                           \
{                                                                             \
   return kernel;                                                             \
};                                                                            \
                                                                              \
virtual std::string getKernelName()                                           \
{                                                                             \
   return #name ;                                                             \
}

#define CREATE_KERNEL_CLASS(ClassName, KernelFunction, KernelCode)            \
class ClassName : public ClKernel                                             \
{                                                                             \
   CLKERNEL(KernelFunction, KERNEL(KernelCode));                              \
};