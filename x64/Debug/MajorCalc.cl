kernel void MajorCalc(global const float* dataSet, global float* outputSet)
{
	int id = get_global_id(0);
	float current = dataSet[id];

	current = current / 3.141592f;

	current = sin(current);

	current = tan(current);

	if (current < 0)
		current = current * (-1);

	for (int n = 0; n < id; n++)
		current = sqrt(current) * sqrt(current);

	outputSet[id] = current;
}
